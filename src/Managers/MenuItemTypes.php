<?php

namespace Skimia\Menus\Managers;

use Illuminate\Support\Collection;
use Skimia\Menus\Data\Models\Item\MenuItem;

class MenuItemTypes
{

    /**
     * @var Collection
     */
    protected $_types;

    /**
     * @var Collection
     */
    protected $_links;


    function __construct(){

        $this->_types = new Collection();
        $this->_links = new Collection();
    }

    public function add( $className ){
        $desc = $className::describe();
        $this->_types[$desc['sysName']] = $desc;

        return $this;

    }

    /**
     * @param $name
     * @return Collection
     */
    protected function getCategory($name){
        if(!$this->_links->has($name))
            $this->_links->put($name,new Collection());
        return $this->_links->get($name);
    }

    public function addLink($category, $sysName, $meta, $overInfos = []){
        $category = $this->getCategory($category);

        $category->push([
            'sysName' => $sysName,
            'meta' => $meta,
            'info' => array_merge($this->_types->get($sysName),$overInfos)
        ]);

        return $this;
    }

    public function getAllLinks( ){

        \Event::fire('skimia.menus::generate.links');

        return $this->_links->toArray();

    }

    public function getTypes(){
        return $this->_types;
    }

    public function getClass($name){
        return $this->_types->get($name)['class'];
    }
}