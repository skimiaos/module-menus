<?php

namespace Skimia\Menus;


use Skimia\Menus\Facades\MenuItemTypes;
use Skimia\Menus\Data\Models\Item\LinkMenuItem;
use Skimia\Menus\Data\Models\Item\PageMenuItem;
use Skimia\Modules\ModuleBase;
use Skimia\Pages\Data\Models\Page;


class Module extends ModuleBase{



    public function getAliases(){
        return [
            'MenuItemTypes'=> MenuItemTypes::class
        ];
    }
} 