<?php

namespace Skimia\Menus\Controllers;

use Angular;
use Controller;
use Input;
use Skimia\Menus\Data\Models\Item\MenuItem;
use MenuItemTypes;

class MenuItems extends Controller{


    public function save(){
        $input =  Input::get('tree');
        $root =  Input::get('root');

        foreach ($input as $menuitem) {

            /**
             * @var $item \Skimia\Menus\Data\Models\Item\MenuItem
             */
            $item = MenuItem::findOrFail($menuitem['id']);

            if(isset($menuitem['parent_id']))
                $item->parent_id = intval($menuitem['parent_id']);
            else
                $item->parent_id = $root;

            $item->order = $menuitem['order'];
            $item->meta = $menuitem['meta'];
            $item->name = $menuitem['name'];
            $item->title = $menuitem['title'];
            if(isset($menuitem['html_attributes']))
                $item->html_attributes = $menuitem['html_attributes'];
            else
                $item->html_attributes = [];

            $item->save();

        }
        \AResponse::addMessage('Arbre sauvegardé');
        return \AResponse::r(['success'=>true]);
    }

    public function remove(){
        $this->save();
        $input =  Input::get('id');
        $item = MenuItem::findOrFail($input);

        $item->delete();

        \AResponse::addMessage('Element supprimé');
        return \AResponse::r(['success'=>true]);

    }

    public function makeItem(){
        $input =  Input::only(['parent_id','order','systemName','meta','title']);

        $item = new MenuItem();
        $item->parent_id = $input['parent_id'];
        $item->meta = $input['meta'];
        $item->order = $input['order'];
        $item->class_name = MenuItemTypes::getClass($input['systemName']);
        $item->name = $input['title'];
        if(Input::has('name')){
            $item->title = Input::get('name');
        }else{
            $item->title = $input['title'];
        }

        $item->save();

        \AResponse::addMessage('Element crée');
        return \AResponse::r(['entity'=>array_merge(['childs'=>[]],$item->toArray())]);

    }

} 