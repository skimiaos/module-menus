<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 23/11/2014
 * Time: 09:28
 */

namespace Skimia\Menus\Facades;

use \Illuminate\Support\Facades\Facade;

class MenuItemTypes extends Facade{

    protected static function getFacadeAccessor(){
        return 'Skimia\Menus\Managers\MenuItemTypes';
    }
} 