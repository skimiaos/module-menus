<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 05/03/2015
 * Time: 15:15
 */
namespace Skimia\Menus\Components;

use Illuminate\Support\Collection;
use Skimia\Pages\Components\Component;
use Skimia\Menus\Data\Models\Menu as MenuEntity;
class Menu extends Component{

    protected static $systemName = 'menu';

    protected $name = 'Menu';
    protected $description = 'affiche un menu';
    protected $icon = 'os-icon-flow-tree';

    protected $show_template = 'skimia.menus::components.menu';


    protected function makeFields(){

        $this->fields = [
            'menu_name'=>[
                'type'=>'select',
                'label'=>'choix du slider',
                'choicesFct'=> function(){
                    return MenuEntity::lists('name','id');
                }
            ],
            'maxDepth'=>[
                'type'=>'text',
                'label'=>'Profondeur du menu (1 signifie menu > sous menu)',
                'default'=>'0'
            ],
        ];
        $this->fields['_identifier']= ['type'=>'text','label'=>'Identifier','required'];
        $this->fields = new Collection($this->fields);
        $this->fieldsMaked = true;
        return $this;
    }

    public function onShow($merge_config = array())
    {
        $merged = $this->position->getConfiguration();

        $merged['root'] = MenuEntity::find($merged['menu_name'])->root;
        $merge_config = array_merge ( $merge_config, $merged ) ;

        return $merge_config;
    }

    protected $fields  = [

    ];

    public function getStaticJS()
    {
        return '';
    }

    public function getDynJS()
    {
        //dd($this->onJavascript());
        return '';
    }

    public function getStaticCSS()
    {
        return '';
    }

    public function getDynCSS()
    {
        return '';
    }
}