<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 05/03/2015
 * Time: 15:15
 */
namespace Skimia\Menus\Components;

use Illuminate\Support\Collection;
use Skimia\Pages\Components\Component;
use Skimia\Menus\Data\Models\Menu as MenuEntity;

class BreadCrumb extends Component{

    protected static $systemName = 'breadcrumb';

    protected $name = 'Breadcrumb';
    protected $description = 'affiche le fil d\'ariane';
    protected $icon = 'os-icon-flow-line';

    protected $show_template = 'skimia.menus::components.breadcrumb';


    protected function makeFields(){

        $this->fields = [
            'menu_name'=>[
                'type'=>'select',
                'label'=>'choix du slider',
                'choicesFct'=> function(){
                    return MenuEntity::lists('name','id');
                }
            ],
        ];
        $this->fields['_identifier']= ['type'=>'text','label'=>'Identifier','required'];
        $this->fields = new Collection($this->fields);
        $this->fieldsMaked = true;
        return $this;
    }

    public function onShow($merge_config = array())
    {
        $merged = $this->position->getConfiguration();

        $merged['root'] = MenuEntity::find($merged['menu_name'])->root;
        $merge_config = array_merge ( $merge_config, $merged ) ;

        return $merge_config;
    }

    protected $fields  = [

    ];

    public function getStaticJS()
    {
        return '';
    }

    public function getDynJS()
    {
        //dd($this->onJavascript());
        return '';
    }

    public function getStaticCSS()
    {
        return '';
    }

    public function getDynCSS()
    {
        return '';
    }
}