<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MenusMenus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('menus_menus')){
			Schema::create('menus_menus', function(BluePrint $table){
				$table->increments('id');
				$table->integer('root_item_id')->unsigned();

				$table->string('name',255);

				$table->text('meta');

				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menus_menus');
	}

}
