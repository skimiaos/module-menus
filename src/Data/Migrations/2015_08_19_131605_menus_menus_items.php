<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MenusMenusItems extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('menus_items')){
			Schema::create('menus_items', function(BluePrint $table){
				$table->increments('id');
				$table->string('class_name')->index();
				$table->integer('parent_id')->unsigned();
				$table->integer('order')->default(0);

				$table->string('name',255);
				$table->string('title',255);
				$table->text('html_attributes');

				$table->text('meta');

				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menus_items');
	}

}
