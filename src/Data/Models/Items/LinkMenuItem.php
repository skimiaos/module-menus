<?php

namespace Skimia\Menus\Data\Models\Item;



class LinkMenuItem extends MenuItem{




    protected $defaultMeta = [
        'url'=>'#'
    ];

    public function getLink()
    {
        return $this->meta['url'];
    }

    public function setUrl($url){
        $this->meta = [
            'url'=> $url
        ];
        return $this;
    }

    protected static $_sysName = 'link';
    protected static $_icon = 'os-icon-link-2';
    protected static $_name = 'Lien Exterieur';
    protected static $_desc = 'lien depuis votre site vers un autre';

    public static function getFields(){
        return [
            'url'=>['type'=>'text','label'=>'Lien']
        ];
    }

    protected function _getUrl(){
        return $this->meta['url'];
    }

}