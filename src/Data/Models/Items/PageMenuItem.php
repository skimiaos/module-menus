<?php

namespace Skimia\Menus\Data\Models\Item;



use Skimia\Pages\Data\Models\Page;

class PageMenuItem extends MenuItem{




    protected $_Page = false;

    protected $defaultMeta = [
        'page_id'=>'0'
    ];

    public function getLink()
    {
        if($this->meta['page_id'] == 0){
            return '#';
        }
        if(!$this->_Page)
            $this->_Page = Page::findOrFail($this->meta['page_id']);

        if($this->_Page->url == '/')
            return route('skimia.pages::page_home');

        return route('skimia.pages::page_link',[
            'pageUrl'=>$this->_Page->url
        ]);
    }

    public function setPage(Page $page){
        $this->meta = [
            'page_id'=> $page->id
        ];
        return $this;
    }

    protected static $_sysName = 'page';
    protected static $_icon = 'os-icon-docs';
    protected static $_name = 'Lien Interne > Page';
    protected static $_desc = 'lien vers une page de votre site';

    public static function getFields(){
        return [
            'page_id'=>['type'=>'select','label'=>'Lien','choicesFct'=>function(){
                return Page::lists('name','id');
            }]
        ];
    }

    protected function _getUrl(){
        return route('skimia.pages::page_link',['pageUrl'=>trim(Page::findOrFail($this->meta['page_id'],['url'])->url,'/')]);
    }

}