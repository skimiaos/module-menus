<?php

namespace Skimia\Menus\Data\Models;

use Eloquent;

class Menu extends Eloquent{

    protected $table = 'menus_menus';



    public function root(){
        return $this->belongsTo('Skimia\Menus\Data\Models\Item\MenuItem','root_item_id');
    }

}