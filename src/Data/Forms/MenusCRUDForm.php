<?php

namespace Skimia\Menus\Data\Forms;

use Eloquent;
use Skimia\Angular\Form\CRUD\ActionOptionsInterface;
use Skimia\Angular\Form\CRUD\Actions\Create\CreateCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Delete\DeleteRestActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Edit\EditCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Get\GetCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudFilterConfiguration;
use Skimia\Backend\Data\Models\Dashboard;
use Skimia\Auth\Traits\Acl;

use Skimia\Angular\Form\CRUD\CRUDForm;
use Skimia\Angular\Form\CRUD\Options;
use Orchestra\Model\Role;
use Skimia\Angular\Form\CRUD\OptionsInterface;
use Skimia\Menus\Data\Models\Item\LinkMenuItem;
use Skimia\Menus\Data\Models\Menu;
use Skimia\Pages\Data\Models\Page;

class MenusCRUDForm extends CRUDForm{

    use Acl;
    use ListCrudActionTrait;
    use EditCrudActionTrait;
    use DeleteRestActionTrait;
    use CreateCrudActionTrait;

    /**
     * @return Eloquent
     */
    protected function getNewEntity()
    {
        return new Menu();
    }

    protected function configure(OptionsInterface $options)
    {
        //First if you want use automatic Translation, set translation context
        $options->setTranslationContext('skimia.menus::form.menus');
        //Second set global options
        $options->Access()->simpleAccess(false);


        $options->Fields()->makeTextField('name')
            ->transAll()
            ->setDisplayOrder(1000);


        $this->registerCRUDFormEvent('afterSave',function(Menu $entity){

            if(!$entity->root){
                $root = new LinkMenuItem();
                $root->save();
                $entity->root()->associate( $root );
                //dd(get_class($entity));
                $entity->save();
            }
        });

        $this->registerOverrideVarEvent('rest-get-edit-query',function($queryBuilder){

            return $queryBuilder->with('root');
        });

        $this->registerOverrideVarEvent('rest-get-edit-response',function($entity){

            $entity['links'] = \MenuItemTypes::getAllLinks();

            $entity['form'] = array_merge($entity['form'],[
                'meta.page_id*selectItems'=> Page::lists('name','id')
            ]);

            return $entity;
        });

    }

    protected function configureActions(ActionOptionsInterface $options)
    {
        //TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$LIST_REST_ACTION)->setIcon('os-icon-flow-tree')->setTitle('Gestion de vos Menus');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$CREATE_REST_ACTION)->setIcon('os-icon-flow-tree')->setTitle('Créer un nouveau Menu');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$EDIT_REST_ACTION)->setIcon('os-icon-flow-tree')->setTitle('Editer votre Menu');//TRANSFO RELATIONNELLES
        //$options->ActionFields(self::$LIST_REST_ACTION)->makeRelationField('roles')->ManyToManyRelation()->displayColumns(['name']);

        $this->createConfiguration->redirectToEditAfter();
        $this->listConfiguration->enableAutoRedirect(MenusCRUDForm::getNew(),'edit');

        $this->listConfiguration->setPaginationPageSizes(2,[2,4,8,10]);
        $this->listConfiguration->addIdColumn();
        $name = $this->listConfiguration->getNewColumnDefinition('name')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName();

        $options->ActionFlash(self::$EDIT_REST_ACTION)->setForContext('editSave','Menu "%name%" edité');
        $options->ActionFlash(self::$CREATE_REST_ACTION)->setForContext('createSave','Nouveau Menu "%name%" crée');

        $options->ActionFlash(self::$DELETE_REST_ACTION)->setForContext('deleteDone','Menu "%name%" supprimé','warning',5000);
        $this->listConfiguration->setEmptyMessage('Pour créer de nouveaux menus veuillez cliquer sur le bouton Ajouter ci-dessus','Aucun Menu disponnible');

        $this->createConfiguration->redirectToEditAfter();

        $options->ActionTemplate(self::$EDIT_REST_ACTION)->bindBlade('skimia.menus::form.edit-menus');


    }

    /**
     * @return string
     */
    public function getCRUDName()
    {
        return 'menus';
    }

}