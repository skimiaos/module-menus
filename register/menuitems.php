<?php

MenuItemTypes::add(\Skimia\Menus\Data\Models\Item\LinkMenuItem::class);
MenuItemTypes::add(\Skimia\Menus\Data\Models\Item\PageMenuItem::class);


Event::listen('skimia.menus::generate.links',function(){

    $pages = \Skimia\Pages\Data\Models\Page::all();

    foreach ($pages as $page) {
        MenuItemTypes::addLink('pages','page',[
            'page_id'=>$page->id
        ],[
            'name'=>''.$page->name,
            'title'=> empty($page->description)? $page->name:$page->description
        ]);
    }


});