<?php
use Skimia\Backend\Managers\Bridge;

Hook::register('activities.pages_manager.sidenav-item',function(Bridge $bridge){



    $bridge->items = [
        'menuss'=>[
            'icon'        => 'os-icon-flow-tree',
            'name'        => 'Menus',
            'type'        => 'state',
            'state'       => 'menu_manager.menus*list',
            'color'       => 'light-blue lighten-1',
            'stateParams' => '{}'
        ]
    ];
},1500);