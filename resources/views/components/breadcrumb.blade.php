

<?php
        $bread = [];
        $element = $root;
        while($element !== false){

            $bypass = false;
            foreach($element->childs as $subItem){
                if($subItem->isActive()){
                    $bread[] = $subItem;
                    $element = $subItem;
                    $bypass = true;
                    break;
                }
            }
            if(!$bypass)
                $element = false;

        }


?>
    <ul>

        <li><a href="{{url()}}" title="Retourner a la page d'Accueil">Accueil</a></li>
    @foreach($bread as $element)
            @if($bread[count($bread)-1] == $element)
                <li class="active" >{{$element->name}}</li>
            @else
                <li><a href="{{$element->getUrl()}}" title="{{$element->title}}">{{$element->name}}</a></li>
            @endif


    @endforeach

    </ul>