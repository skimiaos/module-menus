<?php
$i = 0;
?>
@foreach($root->childs as $item)
    @if(count($item->childs) >0 && $maxDepth > 0)
        <ul id="sub-menu-{{$i}}" class="dropdown-content">
            @foreach($item->childs as $subItem)
                <li><a href="{{$subItem->getUrl()}}" title="{{$subItem->title}}">{{$subItem->name}}</a></li>
            @endforeach
        </ul>
    @endif
    <?php
    $i++;
    ?>
@endforeach



<?php
$i = 0;
?>
<ul class="right hide-on-med-and-down">
    @foreach($root->childs as $item)
        @if(count($item->childs) > 0 && $maxDepth > 0)
            <li @if($item->isActive()) {{$item->getAttributesHasHTML(['class'=>'active'])}}@else {{$item->getAttributesHasHTML()}}@endif><a title="{{$item->title}}" class="dropdown-button" href="#!" data-activates="sub-menu-{{$i}}">{{$item->name}}<i class="mdi-navigation-arrow-drop-down right"></i></a></li>
        @else
            <li @if($item->isActive()) {{$item->getAttributesHasHTML(['class'=>'active'])}}@else {{$item->getAttributesHasHTML()}}@endif><a href="{{$item->getUrl()}}" title="{{$item->title}}">{{$item->name}}</a></li>
        @endif

        <?php
        $i++;
        ?>
    @endforeach
</ul>