@extends('skimia.angular::form.crud.actions.form',[ 'action' => 'edit'])


@block('page.scripts')

        <!-- Nested node template -->
<script type="text/ng-template" id="nodes_renderer.html">
    <div class="angular-item" ng-show="node.hide" style="line-height: 50px">
        <div class="progress" style="display:inline-block;border-radius: 0px;margin:0;">
            <div class="indeterminate"></div>
        </div>
    </div>
    <div class="angular-item" ng-hide="node.hide">
        @foreach(MenuItemTypes::getTypes() as $type)
        <span {{os_tooltip($type['desc'],'right')}} ng-show="node.class_name == '{{addslashes($type['class'])}}'"><i class="{{$type['icon']}}"></i></span>
        @endforeach
            <span data-nodrag {{os_tooltip('Editer','right')}} ng-click="node.editing = !node.editing" ng-init="node.editing = angular.isDefined(node.editing) ? node.editing : false"><i class="os-icon-cog-1"></i></span>
        <input style="max-width:50%;margin:0;border-bottom: none;" type="text" ng-model="node.name" {{os_tooltip('Cliquer pour modifier le titre','right')}}/>
        <a data-nodrag
              class="right"
              ng-click="removed(this,node)">
            <i class="os-icon-trash-1 red-text"></i>
        </a>
        <os-container ng-show="node.editing" class="editing" ng-init="initNode(node)" direction="column">
            <os-container os-flex>
            {{AngularFormHelper::render('text','title', ['formName'=>'node','label'=>'titre SEO','type'=>'text'])}}
            </os-container>
            @foreach(MenuItemTypes::getTypes() as $type)
                <os-container os-flex ng-show="node.class_name == '{{addslashes($type['class'])}}'">
                    @foreach($type['fields'] as $key=>$field)
                        {{AngularFormHelper::render($field['type'],'meta.'.$key, array_merge(['formName'=>'node'],$field))}}
                    @endforeach
                </os-container>
            @endforeach
            <os-container os-flex>
                {{AngularFormHelper::render('associative','html_attributes', ['formName'=>'node','label'=>'Attributs HTML','type'=>'associative','fields'=>[
                    'class'=>['type'=>'text','label'=>'Classes CSS','formName'=>'node'],
                    'id'=>['type'=>'text','label'=>'ID','formName'=>'node']
                ]])}}
            </os-container>
        </os-container>
    </div>
    <ul ui-tree-nodes="" ng-model="node.childs">
        <li ng-repeat="node in node.childs" ui-tree-node ng-include="'nodes_renderer.html'">
        </li>
    </ul>
</script>

<script type="text/ng-template" id="nodes_renderer1.html">
    <div {{os_tooltip('[[node.desc ]]','left')}}>
    <div ui-tree-handle class="os-toolbox-item tree-node tree-node-content">

        <i class="@{{ node.icon }}"></i>
        <p>@{{node.name}}</p>
                </div>
    </div>
</script>

<script type="text/ng-template" id="nodes_renderer2.html">
    <div {{os_tooltip('[[ node.info.desc ]]','left')}}>
    <div ui-tree-handle class="os-toolbox-item tree-node tree-node-content">
        <i class="@{{ node.info.icon }}"></i>
        <p>@{{node.info.name}}</p>
    </div>
        </div>
</script>

<style type="text/css">


    .menu-tree .angular-ui-tree {
        padding: 0 0.75rem;
    }

    .menu-tree .input-field label.active, .menu-tree .os-input label.active, .menu-tree .os-select label.active {
        -webkit-transform: translateY(-90%);
        transform: translateY(-90%);
    }
    .menu-tree .angular-ui-tree-empty {
        box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        min-height: 120px;
        background-color: #e5e5e5;
        background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff), -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
        background-image: -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff), -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
        background-image: linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff), linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
        background-size: 60px 60px;
        background-position: 0 0, 30px 30px;
    }
    .menu-tree .angular-ui-tree-empty:before {
        content:'Ce menu est vide, pour ajouter de nouveaux éléments, veuillez ouvrir le paneau a droite à l\'aide du bouton gris indiqué d\'une flêche et glisser déposer un élément sur la partie cadrillée, pour valider l\'ajout, cette partie doit disparaître';
        margin: 10px;
        padding: 5px;
        display: inline-block;
        background-color: rgba(255,255,255,0.8);
        margin-right: 200px;
        font-size: 150%;
    }
    .menu-tree .angular-ui-tree-empty:after {
        position: absolute;
        right: 0;
        top: 0;
        content: '\e880';
        font-family: "fonticons";
        font-style: normal;
        font-weight: normal;
        speak: none;
        display: inline-block;
        text-decoration: inherit;
        width: 1em;
        margin-right: .2em;
        text-align: center;
         opacity: .8;
        font-variant: normal;
        text-transform: none;
        line-height: 1em;
        margin-left: .2em;
         font-size: 200%;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        text-shadow: 1px 1px 1px rgba(127, 127, 127, 0.3);

    }

    .angular-ui-tree-nodes {
        position: relative;
        margin: 0;
        padding: 0;

        list-style: none;
    }

    .menu-tree .angular-ui-tree-nodes .angular-ui-tree-nodes {

    }

    .menu-tree .angular-ui-tree-node, .angular-ui-tree-placeholder, .angular-ui-tree-drag .angular-ui-tree-node{
        position: relative;
        margin: 5px;
        padding-left: 75px;


        background-color: #546e7a;
        box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
    }
    .menu-tree .angular-ui-tree-node .angular-item, .angular-ui-tree-placeholder .angular-item, .angular-ui-tree-drag .angular-ui-tree-node .angular-item{
        padding-left: 10px;
        padding-right: 10px;

        background-color: #fff;

        font-family: Roboto, sans-serif;
        font-size: 18px;
        line-height: 50px;
        font-weight: 300;
    }
    .menu-tree .angular-ui-tree-hidden {
        display: none;
    }

    .menu-tree .angular-ui-tree-placeholder {
        margin-left:75px;
        padding: 0;
        min-height: 30px;
        background-color: #19DC6A;
        min-width: 400px;
    }

    .menu-tree .angular-ui-tree-handle {
        cursor: move;
        text-decoration: none;
        font-weight: bold;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        min-height: 20px;
        line-height: 20px;
    }

    .angular-ui-tree-drag {
        position: absolute;
        pointer-events: none;
        z-index: 999;
        opacity: .8;
        min-width: 400px;
    }

</style>
@endoverride


@block('page.content.afterform')
<os-container direction="row">
    <div class="menu-tree" style="flex:1">
        <div ui-tree="treeOps" >
            <ul class="sTree listsClass" ui-tree-nodes="" ng-model="form.root.childs" id="tree-root">
                <li ng-repeat="node in form.root.childs" ui-tree-node ng-include="'nodes_renderer.html'"></li>
            </ul>
        </div>
    </div>

    
<os-container direction="column" class="os-toolbox">
    <div class="os-toolbox-tab">
        <i class="os-icon-tools"></i>
    </div>
    <h6>Toolbox</h6>
    <div class="os-toolbox-selector">
        <div class="input-field col s12">
            <select ng-model="toolboxSelectedItem" ng-init="toolboxSelectedItem = 'pages'">

                <option ng-repeat="(cat,items) in links" value="@{{ cat }}">@{{ ucFirst(cat)}}</option>
                <option value="types">Elements Bruts</option>
            </select>
            <label>Catégories</label>
        </div>
    </div>
    <div ui-tree="tree1Options" data-clone-enabled="true" data-nodrop-enabled="true" class="os-toolbox-items"
         ng-show="toolboxSelectedItem == 'types'">


        <div ui-tree-nodes="" ng-model="itemTypes">
            <li ng-repeat="node in itemTypes" ui-tree-node ng-include="'nodes_renderer1.html'"></li>
        </div>
    </div>

    <div ng-repeat="(cat,items) in links" ui-tree="tree1Options" data-clone-enabled="true" data-nodrop-enabled="true" class="os-toolbox-items"
         ng-show="toolboxSelectedItem == cat">


        <div ui-tree-nodes="" ng-model="items">
            <li ng-repeat="node in items" ui-tree-node ng-include="'nodes_renderer2.html'"></li>
        </div>
    </div>

</os-container>
</os-container>

@endoverride








@Controller
//<script>

    $scope.ucFirst = function (string) {
        return string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase();
    };
    $scope.initNode = function(node){
        //console.log(node);
        if(Array.isArray(node.meta))
            node.meta = {};
    }

    $scope.beforeControllerInit = function() {
        setTimeout(function () {
            $('.os-toolbox-selector select').material_select();
            setTimeout(function () {
                $('.tooltipped').tooltip({delay: 50});
                $('*[os-toolbox]').tooltip();
            }, 1);

            var toolboxTab = $('.os-toolbox-tab');
            toolboxTab.on('click', function () {
                if ($('.os-toolbox').hasClass('os-toolbox--active')) {
                    $('.os-toolbox').removeClass('os-toolbox--active');
                    $('.layout-crud').removeClass('layout-crud--toolbox');
                    toolboxTab.find('i').removeClass('os-icon-right');
                    toolboxTab.find('i').addClass('os-icon-tools');
                }
                else {
                    $('.os-toolbox').addClass('os-toolbox--active');
                    $('.layout-crud').addClass('layout-crud--toolbox');
                    toolboxTab.find('i').removeClass('os-icon-tools');
                    toolboxTab.find('i').addClass('os-icon-right');
                }

            });
        },500);
    }

    var setParentOrderRecursive = function($element,$arr) {

        $arr.push($element);

        if ($element.childs.length > 0){
            var $i = 0;
            angular.forEach($element.childs,function(value,key){
                value.order = $i;
                value.parent_id = $element.id;

                setParentOrderRecursive(value, $arr);
                $i++;
            });
        }

    }

    var dataTypes = {{json_encode(MenuItemTypes::getTypes())}};
    $scope.itemTypes = [];
    angular.forEach(dataTypes, function(value){
        $scope.itemTypes.push(value);
    });

    $scope.treeOps = {
        dragStart:function(event){
            $('*[os-toolbox]').tooltip();
            event.source.nodeScope.$modelValue.editing = false;
        }
    };
    $scope.tree1Options = {
        beforeDrop:function(event){

            if(event.dest.nodesScope.$element.parents('.menu-tree').length == 0)
                return;
            var parent_id = (angular.isDefined(event.dest.nodesScope.$nodeScope) && event.dest.nodesScope.$nodeScope != null )? event.dest.nodesScope.$nodeScope.$modelValue.id:$scope.form.root_item_id;
            var order = event.dest.index;
            var systemName = event.source.nodeScope.$modelValue.sysName;
            if(angular.isDefined(event.source.nodeScope.$modelValue.meta))
            {
                var meta = event.source.nodeScope.$modelValue.meta;
                var title = event.source.nodeScope.$modelValue.info.name;
                var name = event.source.nodeScope.$modelValue.info.title ? event.source.nodeScope.$modelValue.info.title:event.source.nodeScope.$modelValue.info.name;
            }else{
                var meta = {};
                var title = event.source.nodeScope.$modelValue.name;
                var name = event.source.nodeScope.$modelValue.title ? event.source.nodeScope.$modelValue.title:event.source.nodeScope.$modelValue.name;
            }


            $angular_response().post('{{route('skimia.menus::menuitem.makeitem')}}',{ title:title,name:name,parent_id:parent_id, order:order,systemName:systemName,meta:meta}).then(function(data){

                angular.forEach(data.data.entity,function(value,key){
                    event.dest.nodesScope.$modelValue[event.dest.index][key] = value;
                });
                event.dest.nodesScope.$modelValue[event.dest.index].hide = false;
                $('.os-select select').material_select();

                //event.source.nodeScope.$modelValue = DESTINATION;

            });
            //console.log(event);

            event.source.nodeScope.$modelValue.hide = true;
            return true;
        }
    };

    $scope.beforeSave = function(){
        $elements = [];
        setParentOrderRecursive($scope.form.root, $elements);
        return $angular_response().post('{{route('skimia.menus::menuitem.savetree')}}',{ tree:$elements, root:$scope.form.root_item_id});

    };

    $scope.removed = function($element,$node){
        console.log($element);
        if(confirm("Voulez vous vraiment supprimer l\'element \""+$node.name+"\" ainsi que ses enfants ?"))
        {
            $elements = [];
            $node.hide = true;
            setParentOrderRecursive($scope.form.root, $elements);

                $angular_response().post('{{route('skimia.menus::menuitem.remove')}}',{ id:$node.id,tree:$elements, root:$scope.form.root_item_id}).then(function(data){
                    if(data.data.success){
                        $element.remove();
                    }

                });




        }
        console.log($element);
    };


//</script>


@EndController