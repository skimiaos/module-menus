<?php

$app = Angular::get(OS_APPLICATION_NAME);


$app->addState('menu_manager','/menu-manager',function($params){
    return View::make('skimia.pages::activities.pages-manager.index',$params);
});

\Skimia\Menus\Data\Forms\MenusCRUDForm::register($app,'menu_manager');

Route::post('/menuitems/savetree',['as'=>'skimia.menus::menuitem.savetree','uses'=>'Skimia\Menus\Controllers\MenuItems@save']);
Route::post('/menuitems/remove',['as'=>'skimia.menus::menuitem.remove','uses'=>'Skimia\Menus\Controllers\MenuItems@remove']);
Route::post('/menuitems/makeItem',['as'=>'skimia.menus::menuitem.makeitem','uses'=>'Skimia\Menus\Controllers\MenuItems@makeItem']);